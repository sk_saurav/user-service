package com.creative.userservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creative.userservice.pojo.UserInfo;
import com.creative.userservice.service.UserService;

@RestController
@RequestMapping(name = "user", value = "/user", path = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/findUserByID/{userID}")
    public UserInfo findUserById(@PathVariable("userID") final Long userID) {
        return userService.findUserById(userID);
    }

    @GetMapping("/findFibonacci/{num}")
    public int fibonacci(@PathVariable("num") final int num) {
        if (num == 0) {
            return 1;
        }
        if (num == -1) {
            return 0;
        }
        else
            return fibonacci(num-1) + fibonacci(num-2);
    }
}
