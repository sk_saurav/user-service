/*
 * Copy right from Saurav kumar
 */

package com.creative.userservice.common.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Arrays;

import static org.springframework.http.HttpMethod.*;


@Component
@EnableWebFlux
public class SimpleCorsFilter implements WebFilter {
    private final Logger log = LoggerFactory.getLogger(SimpleCorsFilter.class);

    public SimpleCorsFilter() {
        log.info("SimpleCORSFilter init");
    }

    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
        serverWebExchange.getResponse()
                .getHeaders()
                .setAccessControlAllowOrigin("*");
        serverWebExchange.getResponse()
                .getHeaders()
                .setAccessControlAllowMethods(Arrays.asList(POST, PUT, GET, OPTIONS, DELETE));
        serverWebExchange.getResponse()
                .getHeaders()
                .setAccessControlMaxAge(3600);
        serverWebExchange.getResponse()
                .getHeaders()
                .setAccessControlAllowCredentials(true);
        return webFilterChain.filter(serverWebExchange);
    }
}
