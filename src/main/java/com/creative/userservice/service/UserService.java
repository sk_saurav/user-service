package com.creative.userservice.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.creative.userservice.pojo.UserInfo;
import com.creative.userservice.repository.UserRepository;

@Service
public class UserService {
	private final Logger log = LoggerFactory.getLogger(UserService.class);

	private UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public UserInfo findUserById(Long userID) {
		log.info("Entering into findUserById: {}", userID);
		Optional<UserInfo> userInfo = userRepository.findById(userID);
		UserInfo info = null;
		if (userInfo.isPresent()) {
			info = userInfo.get();
		}
		log.info("Exiting from findUserById: {}", info);
		return info;
	}
}
